# Django Tutorial Poll Application.
source -> [tutorial](https://docs.djangoproject.com/en/2.2/intro/)

## Get Started

` git clone git@gitlab.com:koushinto/poll.git' `

```
cd ./poll
pip install -r requirements.txt
python manage.py migrate
```

## Create Admin User

` python manage.py createsuperuser `

## Run server

` python manage.py runserver `
